interface IProgressController {
    void updateProgress(int progressDone, String progressMessage) throws Exception;
    void updateProgress(int progressDone) throws Exception;
}

public class Progress implements IProgressController {

  private int counter;

  public Progress(){
	counter = 0;
  }

  public void updateProgress(int progressDone, String progressMessage) throws Exception {
      //System.out.println("Llamada a updateProgress con: " + progressDone + ", " + progressMessage);
      if(++counter == 2){
        System.out.println("Llamada a updateProgress con: " + progressDone + ", " + progressMessage);
	//throw new Exception("Cancelado por el usuario");
      }
  }

  public void updateProgress(int progressDone) throws Exception {
      /*System.out.println("Llamada a updateProgress con: " + progressDone);
      if(++counter == 3){
	throw new Exception("Cancelado por el usuario");
      }*/
  }
}
