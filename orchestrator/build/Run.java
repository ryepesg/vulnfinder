import transformation.core;
import java.util.*;
import org.zaproxy.clientapi.core.ApiResponse;
import org.zaproxy.clientapi.core.ApiResponseElement;
import org.zaproxy.clientapi.core.ClientApi;

public class Run {
  public static void main(String[] args) throws Exception {
	String pathIn = "./model.s";
	String pathOut = "generated/";
	String proxyURL = "localhost";
	String proxyPort = "8080";

	if (args.length == 1) {
	  System.out.println("WARNING: Using default paths");
	}
	else if (args.length > 1) {
	  pathIn = args[0];
	  pathOut = args[1];
    }
	else if (args.length > 3) {
	  proxyURL = args[2];
	  proxyPort = args[3];
	}
	System.out.println("Answer from core.start: " + new core().start(pathIn, pathOut, new Progress(), proxyURL, Integer.parseInt(proxyPort)));
  }
}
